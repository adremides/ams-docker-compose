# ams-docker-compose

Ant Media Server docker-compose files

## Usage
Download with
``` bash
git clone https://gitlab.com/adremides/ams-docker-compose.git
```
Then start with
``` bash
docker-compose up -d
```

## Requirements
Docker

docker-compose

## Thanks to
chauffer for making the image